#include <stdio.h>
int input()
{
   int n;
   printf("Enter number of elements\n");
   scanf("%d",&n);
   return n;
}
void input1(int n,int array[200])
{
    int i=0;
    printf("Enter the array elements in accending order\n");
    for(i=0;i<n;i++)
       scanf("%d",&array[i]);
}
int input2()
{
    int search;
    printf("Enter the value to find\n");
    scanf("%d",&search);
    return search;
}
int compute(int n,int search,int array[])
{
    int first,last,middle;
    first=0;
    last=n-1;
    middle=(first+last)/2; 
    while (first<=last) 
    {
      if(array[middle]<search)
        first=middle+1;    
      else if(array[middle]==search)
      {
        return middle; 
        break;
      }
      else
        last=middle-1;
      middle=(first+last)/2;
   }
   if (first>last)
      return 0;
}
void output(int p,int search)
{
    if(p>0)
    printf("%d found at location %d.\n",search,p+1);
    if(p==0)
    printf("Not found! %d isn't present in the list.\n",search); 
}
int main()
{  
    int n,search,first,last,middle,p;
    n=input();
    int array[n];
    input1(n,&array[n]);
    search=input2();
    p=compute(n,search,&array[n]);
    output(p,search);
    return 0;  
}
