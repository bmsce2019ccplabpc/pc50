#include<stdio.h>
struct frac
{
  int num, den;
};
int input1 ()
{
  int n;
  printf ("Enter the number of fractions you want to add\n");
  scanf ("%d", &n);
  return n;
}

void input2 (int n, struct frac f[n])
{
  for (int i = 0; i < n; i++)
    {
      printf ("Enter the numerator and denominator respectively\n");
      scanf ("%d%d", &f[i].num, &f[i].den);
    }
}

struct frac compute (int n, struct frac f[n])
{
  struct frac p1;
  p1.num = f[0].num;
  p1.den = f[0].den;
  for (int i = 1; i < n; i++)
    {
      p1.num = p1.num * f[i].den + p1.den * f[i].num;
      p1.den = p1.den * f[i].den;
    }
  return p1;
}

int lcm (struct frac p1)
{
  int rem, div, divr;
  if (p1.num > p1.den)
    {
      div = p1.num;
      divr = p1.den;
    }
  else
    {
      div = p1.den;
      divr = p1.num;
    }
  while (divr != 0)
    {
      rem = div % divr;
      div = divr;
      divr = rem;
    }
  return div;
}

void output (int div, struct frac p1)
{
  printf ("\nThe fraction is %d/%d", p1.num / div, p1.den / div);
}

int main ()
{
  int div, n;
  struct frac p1;
  n = input1 ();
  struct frac f[n];
  input2 (n, f);
  p1 = compute (n, f);
  div = lcm (p1);
  output (div, p1);
  return 0;
}
