#include<stdio.h>
#include<math.h>
float input()
{
	 float a;
	 scanf("%f",&a);
	 return a;
}	  
float compute(int x1,int y1,int x2,int y2)
{
     float d;
     d=sqrt(pow((x2-x1),2)+pow((y2-y1),2));
     return d;
}
void output(float x1,float y1,float x2,float y2,float d)
{
	printf("The distance between (%f,%f) and (%f,%f) is %f\n",x1,y1,x2,y2,d); 
}
int main()
{
    int x1,y1,x2,y2,d;
	printf("Enter x1\n");
	x1=input();
	printf("Enter y1\n");
	y1=input();
	printf("Enter x2\n");
	x2=input();
	printf("Enter y2\n");
	y2=input();
	d=compute(x1,y1,x2,y2);
	output(x1,y1,x2,y2,d);
	return 0;
}