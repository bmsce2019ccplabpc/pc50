#include<stdio.h>
struct frac
{
    int n;
    int a[];
};
int input()
{
    typedef struct frac p1;
    printf("Enter the amount of numbers you want to add");
    scanf("%d",&p1.n);
    for(int b=1;b<=p1.n;b++)
   {
    printf("Enter the numbers one by one");
    scanf("%d",&p1.a[b]);
   }
   return p1.a;
}
int compute(int a)
{ 
  typedef struct frac p2;
  int c;
  int sum=0;
  for(c=1;c<=p2.n;c++)
   {
    sum=sum+p2.a[c];
   }
  return sum;
}
void output(int sum)
{
    printf("The sum of the numbers entered is %d",sum);
}
int main()
{
    typedef struct frac p3;
    int a[p3.n]=input();
    int sum=compute(a);
    output(sum);
    return 0;
}