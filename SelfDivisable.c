#include<stdio.h>
struct range
{
    int a,b;
};
int input1()
{
    int r;
    printf("Enter number of ranges\n");
    scanf("%d",&r);
    return r;
}
void input2(int r,struct range R[r])
{
    for(int i=0;i<r;i++)
    {
        printf("Enter the range from smaller to bigger respectively\n");
        scanf("%d%d",&R[i].a,&R[i].b);
    }
}
void self(int r,struct range R[r])
{
    for(int i=0;i<r;i++)
    {
        printf("The self divisible numbers between the range %d and %d are:\n",R[i].a,R[i].b);
        for(int x=R[i].a;x<=R[i].b;x++)
        {
            int temp=x;
            while(temp>0)
            {
                int y=temp%10;
                if(y==0)
                {
                    temp=1;
                    break;
                }
                if(temp%y==0)
                {
                    temp=temp/10;
                }
                else
                break;
            }
            if(temp==0)
                printf("%d\n",x);
        }
    }
}
int main()
{
    int r=input1();
    struct range R[r];
    input2(r,R);
    self(r,R);
    return 0;
}
