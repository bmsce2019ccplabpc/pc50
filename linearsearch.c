#include<stdio.h>
int input()
{
    int n;
    printf("Enter the number of elements:\n");
    scanf("%d",&n);
    return n;
}
int input1(int n)
{
    int a[n];
    for(int i=0;i<n;i++)
    {
        printf("Enter the array:\n");
        scanf("%d",&a[i]);
    }
    return a;
}
int input2()
{
    int key;
    printf("Enter the key element:\n");
    scanf("%d",&key);
    return key;
}
int compute(int n,int a[n],int key)
{
    int pos=-1;
    for(int i=0;i<n;i++)
    {
        if(key==a[i])
        {
            pos=i;
            break;
        }
    }
    return pos;
}
void output(int pos)
{
    if(pos>=0)
    printf("Element found at pos %d",pos);
    else
    printf("Element not found");
}
int main()
{
    int n,key,pos;
    n=input();
    int a[n];
    a[n]=input1(n);
    key=input2();
    pos=compute(n,a,key);
    output(pos);
 }
